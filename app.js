var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var Web3 = require('web3');
var MyContractJSON =require(path.join(__dirname, 'build/contracts/Auction.json'))
web3=new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
contractAddress = MyContractJSON.networks['5777'].address;

console.log(contractAddress);

// get abi
const abi = MyContractJSON.abi;

// creating contract object
MyContract = new web3.eth.Contract(abi, contractAddress);
var auctionRouter = require('./routes/getAuctionDetails');
var bidForRouter = require('./routes/bidFor');
var getBidRouter = require('./routes/getBid');
var getBidBackRouter = require('./routes/getBidBack');
var withdrawouter = require('./routes/withdrawBid');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


app.use('/', auctionRouter);
app.use('/bidFor', bidForRouter);
app.use('/getBid', getBidRouter);
app.use('/getBidBack', getBidBackRouter);
app.use('/withdrawBid', withdrawouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
